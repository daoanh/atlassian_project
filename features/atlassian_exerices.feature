Feature: Checking issue functional

@create_new_issue
Scenario: User can crete a new issue
  Given I visit https://jira.atlassian.com/secure/Dashboard.jspa
  And I login with my account
  And I create new issue with summary is vydddf
  Then I should see successful message

@search_issue
Scenario: User can search a issue
  Given I visit https://jira.atlassian.com/secure/Dashboard.jspa
  And I login with my account
  And I go search page issue
  And I search issue that has summary is vydddf
  Then I should see the issue in result list

@update_issue
Scenario: User can search a issue
  Given I visit https://jira.atlassian.com/secure/Dashboard.jspa
  And I login with my account
  And I go search page issue
  And I search issue that has summary is vydddf
  Then I should see the issue in result list
  And I edit title of this issue
 Then I should see the title is updated

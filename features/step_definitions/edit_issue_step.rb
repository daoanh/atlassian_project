When /^I edit title of this issue$/ do
  on(SearchPage).go_to_edit_issue_link
  on(EditIssuePage).wait_until {@current_page.loaded?}
  sleep(3)
  on(EditIssuePage).edit_button
  sleep 3
  @title = 'Updated Issues'
  on(EditIssuePage).edit_issue(@title)
end

When /^I should see the title is updated$/ do
on(EditIssuePage).wait_until {@current_page.loaded?}
sleep(3)
  on(EditIssuePage).has_title_updated(@title).should be_truthy
end
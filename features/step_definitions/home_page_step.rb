Given /^I visit (.*?)$/ do |path|
  @browser.goto  "#{path}"
end

When /^I login with my account$/ do
  on(HomePage).wait_until { @current_page.loaded? }
  on(HomePage).login_link
  on(LoginPage).login
end

When /^I create new issue with summary is (.*?)$/ do |content|
  on(HomePage).wait_until { @current_page.has_create_issue_button? }
  on(HomePage).create_issue_link
  on(CreateIssuePage).create_issue(:summary => content)
end

When /^I go search page issue$/ do
  @browser.goto  "https://jira.atlassian.com/issues/?jql="
  # sleep 5
end

Then /^I should see successful message$/ do
  on(HomePage).wait_until { @current_page.loaded? }
  on(HomePage).should have_successful_messsage
end
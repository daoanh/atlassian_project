When /^I search issue that has summary is (.*?)$/ do |content|
  on(SearchPage).wait_until { @current_page.loaded? }
  on(SearchPage).search(:search_text => content)
end

Then /^I should see the issue in result list$/ do
  on(SearchPage).wait_until { @current_page.loaded? }
  on(SearchPage).should have_issue_in_result_list
end


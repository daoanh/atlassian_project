class HomePage
  include PageObject
  include Header

  div(:dashboard, id: 'dashboard')
  div(:successful_message, css:'.aui-message.aui-message-success.success.closeable.shadowed')

  def loaded?
    dashboard_element.exists?
  end

  def has_create_issue_button?
    create_issue_link_element.exists?
  end

  def has_successful_messsage?
    successful_message_element.visible?
  end
end
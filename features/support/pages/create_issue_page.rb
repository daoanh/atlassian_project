class CreateIssuePage
  include PageObject

  SUMMARY    ||= "Test"

  DEFAULT_DATA ||= {
      summary:          SUMMARY
  }

  div(:create_issue_dialog, id: 'create-issue-dialog')
  text_field(:project_text_field, id: 'project-field')
  div(:issue_type, id: 'issuetype-single-select')
  text_field(:summary_text_field, id: 'summary')
  link(:recent_project, xpath: './/ul[@id="recent-projects"]/li/a')
  link(:first_issue_type, xpath:'.//div[@id="issuetype-suggestions"]/div/ul/li[1]/a')
  button(:create_button, id: 'create-issue-submit')

  def loaded?
    create_issue_dialog_element.visible?
  end

  def create_issue(data = {})
    data = DEFAULT_DATA.merge(data)
    wait_until { self.loaded? }
    project_text_field_element.click
    recent_project_element.when_visible.click
    sleep(3)
    issue_type_element.click
    first_issue_type_element.when_visible.click
    sleep(3)
    self.summary_text_field = data[:summary]
    create_button
    sleep(5)
  end

end
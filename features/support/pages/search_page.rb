class SearchPage
  include PageObject
  include Header

  div(:search_header, id: 'search-header-view')
  text_field(:search_box, id: 'searcher-query')
  button(:search_button, css: '.aui-button.aui-button-subtle.search-button')
  ordered_list(:issue_list, css: '.issue-list')
  link(:issue_link, xpath: './/ol[@class="issue-list"]/li[1]/a')

  DEFAULT_DATA ||= {
      search_text: "",

  }

  def loaded?
    search_header_element.visible?
  end

  def search(data = {})
    data = DEFAULT_DATA.merge(data)
    wait_until { self.loaded? }
    self.search_box = data[:search_text]
    search_button
    sleep(3)
  end

  def has_issue_in_result_list?
     issue_list_element.items > 0
  end

  def go_to_edit_issue_link
    edit_link = issue_link_element.href
    @browser.goto edit_link

  end
end
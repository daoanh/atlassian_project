module Header
  include PageObject

  link(:login_link, xpath: './/li[@id="user-options"]/a')
  link(:create_issue_link, id: 'create_link')
  link(:issues_link, id: 'find_link')
  link(:reported_by_me_link, id: 'filter_lnk_reported_lnk')

end

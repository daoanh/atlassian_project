class LoginPage
  include PageObject
  include Header

  EMAIL_REGISTERED    ||= "luuphamvy87@gmail.com"
  PASSWORD_REGISTERED ||= "h0angvyvy"

  DEFAULT_DATA ||= {
      email:           EMAIL_REGISTERED,
      password:        PASSWORD_REGISTERED,
  }

  text_field(:user_name, id: 'username')
  text_field(:password, id: 'password')
  button(:sign_in, id: 'login-submit')

  def loaded?
    user_name_element.exists?
  end

  def login(data = {})
    data = DEFAULT_DATA.merge(data)
    wait_until { self.loaded? }
    self.user_name = data[:email]
    self.password = data[:password]
    sign_in
  end

end
class EditIssuePage
  include PageObject

  SUMMARY    ||= "Test"

  DEFAULT_DATA ||= {
      summary:          SUMMARY
  }

  link(:edit_button, id: 'edit-issue')
  button(:update_button, id: 'edit-issue-submit')
  text_field(:summary_text_field, id: 'summary')
  h1(:title_h1, :id => 'summary-val')

  def loaded?
    edit_button_element.visible?
  end

  def edit_issue(title)
    wait_until { self.loaded? }
    self.summary_text_field = title
    update_button
    sleep(5)
  end

 def has_title_updated(title)
   title == title_h1_element.text
 end

end